import os
import sys
import time

import numpy as np
from sklearn.datasets import make_blobs

from kmodes.kprotospark import KProtospark
from kmodes.kprototypes import KPrototypes

if __name__ == "__main__":
    total_samples = 10000
    n_features = 3
    k = 3
    X, labels = make_blobs(n_samples=total_samples, n_features=n_features, centers=k)
    output = np.zeros((total_samples, n_features + 1))
    output[:, :n_features] = X
    output[:, n_features] = labels
    start_time_kp = time.time()
    kp = KPrototypes(n_clusters=k, n_init=10)
    kp.fit_predict(output, categorical=[3])
    end_time_kp = time.time() - start_time_kp
    print("KPrototypes took: ", end_time_kp)
    print("KPrototypes centroids: ", kp.cluster_centroids_)
    kps = KProtospark(n_clusters=k, n_partition=4)
    os.environ["PYSPARK_PYTHON"] = sys.executable
    start_time_kps = time.time()
    centroids = kps.fit_predict(output, [3]).centroids
    end_time_kps = time.time() - start_time_kps
    print("KProtospark took: ", end_time_kps)
    print("KProtospark centroids: ", centroids)
    assert end_time_kps < end_time_kp, "KProtospark must be faster than KPrototypes"
