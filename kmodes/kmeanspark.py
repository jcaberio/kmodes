from collections import namedtuple
from typing import List

import numpy as np
import pandas as pd

from pyspark.ml.clustering import KMeans
from pyspark.sql.session import SparkSession
from pyspark.ml.feature import VectorAssembler

from kmodes.kresult import KResult
from kmodes.util.dissim import euclidean_dissim


class KMeanspark:
    def __init__(self, n_clusters: int, max_iter=100):
        self.n_clusters = n_clusters
        self.max_iter = max_iter

    def fit_predict(self, arr: np.ndarray, features: List[str]) -> KResult:
        spark_session = SparkSession.builder.getOrCreate()
        spark_df = spark_session.createDataFrame(pd.DataFrame(arr, columns=features))
        vec_assembler = VectorAssembler(inputCols=spark_df.columns, outputCol="features")
        vector_df = vec_assembler.transform(spark_df)
        kmeans_obj = KMeans(k=self.n_clusters, maxIter=self.max_iter)
        model = kmeans_obj.fit(vector_df)
        centroids = model.clusterCenters()
        cost = model.computeCost(vector_df)
        labels = np.array(model.summary.cluster.collect())
        data_with_labels = np.zeros((arr.shape[0], arr.shape[1] + 1))
        data_with_labels[:, :arr.shape[1]] = arr
        data_with_labels[:, arr.shape[1]:] = labels

        n_rows = arr.shape[0]
        simplified_silhouette = np.zeros(n_rows)

        for i_point in range(n_rows):
            current_point = arr[i_point]
            num_distance = euclidean_dissim(centroids, current_point)
            point_to_centroid_dist = num_distance[np.argpartition(num_distance, 1)[0]]
            point_to_nearest_neighbor_centroid_dist = num_distance[np.argpartition(num_distance, 1)[1]]
            numerator = point_to_nearest_neighbor_centroid_dist - point_to_centroid_dist
            denominator = max(point_to_centroid_dist, point_to_nearest_neighbor_centroid_dist)
            simplified_silhouette[i_point] = numerator / denominator

        ssi = simplified_silhouette.mean()
        result = KResult(data_with_labels=data_with_labels, centroids=centroids, labels=labels, cost=cost, ssi=ssi)
        return result
