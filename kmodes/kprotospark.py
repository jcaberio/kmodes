from collections import namedtuple
from typing import List

import numpy as np
from pyspark import SparkContext

from kmodes.kprototypes import KPrototypes, _split_num_cat
from kmodes.util.dissim import matching_dissim, euclidean_dissim
from kmodes.kresult import KResult


class KProtospark:
    def __init__(self, n_clusters: int, n_partition: int, n_init=10, init='Huang', max_iter=100,
                 num_dissim=euclidean_dissim, cat_dissim=matching_dissim, verbose=0):
        """
        K-Prototypes using Spark RDD
        :param n_clusters:
            The number of clusters to form as well as the number of centroids to generate.
        :param n_partition:
            The number of RDD partitions.
        :param n_init:
            Number of time the k-prototypes algorithm will be run with different centroid seeds.
            The final results will be the best output of n_init consecutive runs in terms of cost.
        :param init:
            Method for initialization:
                'Huang': Method in Huang [1997, 1998]
                'Cao': Method in Cao et al. [2009]
                'random': choose 'n_clusters' observations (rows) at random from data for the initial centroids.
        :param max_iter:
            Maximum number of iterations of the k-modes algorithm for a single run.
        :param num_dissim:
            Dissimilarity function used by the algorithm for numerical variables.
        :param cat_dissim:
            Dissimilarity function used by the kmodes algorithm for categorical variables.
        :param verbose:
             Verbosity mode.
        """
        self.n_clusters = n_clusters
        self.n_partition = n_partition
        self.n_init = n_init
        self.init = init
        self.max_iter = max_iter
        self.num_dissim = num_dissim
        self.cat_dissim = cat_dissim
        self.verbose = verbose

    def fit_predict(self, arr: np.ndarray, categorical: List[int]) -> KResult:
        """
        Predict the closest cluster each sample in arr belongs to.

        :param arr: array-like, shape = [n_samples, n_features]
            New data to predict.
        :param categorical: list of ints
            Index of columns that contain categorical data
        """
        sc = SparkContext(appName="kprotospark")
        try:
            n_rows = arr.shape[0]
            n_cols = arr.shape[1]
            rdd_data = sc.parallelize(arr, self.n_partition)

            def f(iterator):
                l = list()
                for elem in iterator:
                    l.append(elem)
                data = np.asarray(l)
                kp = KPrototypes(n_clusters=self.n_clusters, n_init=self.n_init,
                                 max_iter=self.max_iter, init=self.init,
                                 num_dissim=self.num_dissim, cat_dissim=self.cat_dissim,
                                 verbose=self.verbose)
                kp.fit_predict(data, categorical=categorical)
                yield kp.cluster_centroids_

            result = rdd_data.mapPartitions(f).collect()
            res = np.zeros((self.n_clusters * self.n_partition, n_cols), dtype=object)

            for i in range(len(result)):
                numeric_center = result[i][0]
                categorical_center = result[i][1]
                combined_center = np.zeros((self.n_clusters, n_cols), dtype=object)
                combined_center[:, [int(i_col) for i_col in range(n_cols) if i_col not in categorical]] = numeric_center
                combined_center[:, categorical] = categorical_center
                row_begin_idx = i * self.n_clusters
                row_end_index = row_begin_idx + self.n_clusters
                res[row_begin_idx:row_end_index, :] = combined_center

            kp = KPrototypes(n_clusters=self.n_clusters, n_init=self.n_init,
                             max_iter=self.max_iter, verbose=self.verbose)
            kp.fit_predict(res, categorical=categorical)
            centroids = kp.cluster_centroids_
            Xnum, Xcat = _split_num_cat(arr, categorical)
            gamma = 0.5 * Xnum.std()
            final_results = np.zeros((n_rows, n_cols + 1), dtype=object)
            simplified_silhouette = np.zeros(n_rows)
            for i_point in range(n_rows):
                num_distance = self.num_dissim(centroids[0], Xnum[i_point])
                cat_distance = gamma * self.cat_dissim(centroids[1], Xcat[i_point])
                total_distance = num_distance + cat_distance
                point_to_centroid_dist = total_distance[np.argpartition(total_distance, 1)[0]]
                point_to_nearest_neighbor_centroid_dist = total_distance[np.argpartition(total_distance, 1)[1]]
                numerator = point_to_nearest_neighbor_centroid_dist - point_to_centroid_dist
                denominator = max(point_to_centroid_dist, point_to_nearest_neighbor_centroid_dist)
                simplified_silhouette[i_point] = numerator / denominator
                cluster = np.argmin(total_distance)
                final_results[i_point, :n_cols] = arr[i_point]
                final_results[i_point, n_cols] = cluster

            ssi = simplified_silhouette.mean()
            kprotospark_result = KResult(data_with_labels=final_results,
                                         centroids=kp.cluster_centroids_,
                                         labels=kp.labels_,
                                         cost=kp.cost_,
                                         ssi=ssi)
            return kprotospark_result

        except Exception as e:
            print(e)
        finally:
            sc.stop()
