from typing import NamedTuple, List

import numpy as np


class KResult(NamedTuple):
    data_with_labels: np.ndarray
    centroids: List[np.ndarray]
    labels: np.ndarray
    cost: float
    ssi: float
