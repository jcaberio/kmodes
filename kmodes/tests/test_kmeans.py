import unittest
import numpy as np
from sklearn.datasets import make_blobs
from kmodes.kmeanspark import KMeanspark
from sklearn.cluster import KMeans


class TestKMeans(unittest.TestCase):
    def test_kmeans(self):
        total_samples = 10000
        n_features = 3
        k = 3
        X, labels = make_blobs(n_samples=total_samples, n_features=n_features, centers=k)
        kmeanspark = KMeanspark(k)
        result = kmeanspark.fit_predict(X, ['A', 'B', 'C'])
        kmeans = KMeans(n_clusters=k, random_state=0).fit(X)
        expected = np.array([np.asarray(x) for x in kmeans.cluster_centers_])
        actual = np.array(result.centroids)
        np.testing.assert_allclose(np.sort(expected.flat), np.sort(actual.flat))