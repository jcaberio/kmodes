import unittest
import numpy as np
from sklearn.datasets import make_blobs
from kmodes.kprotospark import KProtospark
from kmodes.kprototypes import KPrototypes


class TestKProtospark(unittest.TestCase):
    def test_kprotospark(self):
        total_samples = 5000
        n_features = 3
        k = 3
        X, labels = make_blobs(n_samples=total_samples, n_features=n_features, centers=k)

        dataset = np.zeros((total_samples, n_features + 1))
        dataset[:, :n_features] = X
        dataset[:, n_features] = labels

        kp = KPrototypes(n_clusters=k, n_init=20)
        kp.fit_predict(dataset, categorical=[3])
        expected_numerical_centroids = np.sort(kp.cluster_centroids_[0].flat)
        print("expected_numerical_centroids: ", expected_numerical_centroids)

        kps = KProtospark(n_clusters=k, n_partition=4, n_init=20)
        actual_numerical_centroids = np.sort(kps.fit_predict(dataset, [3]).centroids[0].flat)
        print("actual_numerical_centroids: ", actual_numerical_centroids)
        np.testing.assert_allclose(expected_numerical_centroids, actual_numerical_centroids, rtol=0.1)
